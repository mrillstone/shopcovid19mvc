using System.Net.Mime;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShopCovid19MCV.Models;
using ShopCovid19MCV.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ShopCovid19MCV.Controllers
{
    public class SubmissionsController : Controller
    {
        private readonly ApplicationDbContext _context;
        public SubmissionsController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult AddBusiness()
        {
            return View();
        }        
        public IActionResult AddNonProfit()
        {
            return View();
        }        
        [HttpGet]
        public IActionResult SubmitPayItForward(int id)
        {
            var biz = _context.Businesses.SingleOrDefault(x => x.Id == id);
            var lunp = _context.NonProfits.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.NonProfitName}).ToList();

            var viewModel = new PayItForwardViewModel
            {
                BusinessId = biz.Id,
                BusinessName = biz.BusinessName,
                LuNp = lunp
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult AddBusiness(Business business)
        {
            _context.Businesses.Add(business);
            _context.SaveChanges();

            return RedirectToAction("Index", "Home");
        }
        
        [HttpPost]
        public IActionResult AddNonProfit(NonProfit nonprofit)
        {
            _context.NonProfits.Add(nonprofit);
            _context.SaveChanges();

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public IActionResult SubmitPayItForward(PayItForward payitforward)
        {
            _context.PayItForwards.Add(payitforward);
            _context.SaveChanges();

            return RedirectToAction("Index", "Home");
        }

    }
}