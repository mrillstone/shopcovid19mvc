using System.ComponentModel.DataAnnotations;

namespace ShopCovid19MCV.Models
{
    public class NonProfit
    {
        public int Id { get; set; }
        public string City { get; set; }
        [Required]
        public string NonProfitName { get; set; }
        public string MailingAddress { get; set; }
        public string ContactName { get; set; }
        
        [Required]
        public string Email { get; set; }
        public string PersonalPhone { get; set; }
        public string NonProfitPhone { get; set; }
        public string Url { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
    }
}