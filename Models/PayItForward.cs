using System.ComponentModel.DataAnnotations;

namespace ShopCovid19MCV.Models
{
    public class PayItForward
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        public string Phone { get; set; }
        public int GiftCardAmount { get; set; }
        [Required]
        public int BusinessId { get; set; }
        [Required]
        public int NonProfitId { get; set; }
    }
}