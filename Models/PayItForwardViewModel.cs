using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ShopCovid19MCV.Models
{
    public class PayItForwardViewModel
    {
        public PayItForward payItForward { get; set; }
        public List<SelectListItem> LuNp { get; set; }
        public string BusinessName { get; set; }
        public int BusinessId { get; set; }
    }
}