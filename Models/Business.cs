using System.ComponentModel.DataAnnotations;

namespace ShopCovid19MCV.Models
{
    public class Business
    {
        public int Id { get; set; }
        public string City { get; set; }
        
        [Required]
        public string BusinessName { get; set; }
        public string ContactName { get; set; }
        
        [Required]
        public string Email { get; set; }
        public string PersonalPhone { get; set; }
        public string BusinessPhone { get; set; }
        public string Url { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public bool GiftCards { get; set; }
        public bool Online { get; set; }
        public bool Curbside { get; set; }
        public bool Delivery { get; set; }
        public bool PIF { get; set; }
    }
}